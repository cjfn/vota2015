﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vota2015.COMUN
{
    public class IngresoUsuario
    {
        public IngresoUsuario()
        { }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
        private int dpi;

        public int Dpi
        {
            get { return dpi; }
            set { dpi = value; }
        }
        private string correo;

        public string Correo
        {
            get { return correo; }
            set { correo = value; }
        }
        private string contraseña;

        public string Contraseña
        {
            get { return contraseña; }
            set { contraseña = value; }
        }
        private string telefono;

        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }
        private int departamento;

        public int Departamento
        {
            get { return departamento; }
            set { departamento = value; }
        }
        private int municipio;

        public int Municipio
        {
            get { return municipio; }
            set { municipio = value; }
        }
        private string direccion;

        public string Direccion
        {
            get { return direccion; }
            set { direccion = value; }
        }

        private string fecha_nac;

        public string Fecha_nac
        {
            get { return fecha_nac; }
            set { fecha_nac = value; }
        }
private int puesto;

public int Puesto
{
    get { return puesto; }
    set { puesto = value; }
}


        public IngresoUsuario(string nombre, string apellido, int dpi, string correo, string contraseña, string telefono, int departamento, int municipio, string direccion, string fecha_nac, int puesto)
{
    this.Nombre = nombre;
    this.Apellido = apellido;
    this.Dpi = dpi;
    this.Correo = correo;
    this.Contraseña = contraseña;
    this.Telefono = telefono;
    this.Departamento = departamento;
    this.Municipio = municipio;
    this.Fecha_nac = fecha_nac;
    this.Puesto = puesto;

}
    }
}