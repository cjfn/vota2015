﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vota2015.COMUN
{
    public class selectcandidatocomun
    {

        public selectcandidatocomun()
        {}

        private int partido;

        public int Partido
        {
            get { return partido; }
            set { partido = value; }
        }
        private int puesto;

        public int Puesto
        {
            get { return puesto; }
            set { puesto = value; }
        }

        private int cod;

        public int Cod
        {
            get { return cod; }
            set { cod = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }




        public selectcandidatocomun(int partido, int puesto, int cod, string nombre, string apellido)
        {
            this.Partido = partido;
            this.Puesto = puesto;
            this.Cod = cod;
            this.Nombre = nombre;
            this.Apellido = apellido;
        }
    }
}