﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vota2015.COMUN
{
    public class comunvoto
    {
        public comunvoto()
        { }

        private string usuario;

        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        private int partido;

        public int Partido
        {
            get { return partido; }
            set { partido = value; }
        }
        private int puesto;

        public int Puesto
        {
            get { return puesto; }
            set { puesto = value; }
        }
        private int candidato;

        public int Candidato
        {
            get { return candidato; }
            set { candidato = value; }
        }
        private int votos;

        public int Votos
        {
            get { return votos; }
            set { votos = value; }
        }
        

        public comunvoto(string usuario, int partido, int puesto, int candidato, int votos)
        {
            this.Usuario = usuario;
            this.Partido = partido;
            this.Puesto = puesto;
            this.Candidato = candidato;
            this.Votos = votos;
        }
    }
}