﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Vota2015.COMUN;

namespace Vota2015.PRESENTACION
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public bool logueado = false;
        protected void Button1_Click(object sender, EventArgs e)
        {
            String resultado = DatosUsuario.iniciarSesion(txtusuario.Text, txtcontraseña.Text);
            if (resultado != "")
            {

                logueado = true;
                string script = "alert('Bienvenido');";
                Session["username"] = txtusuario.Text;
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                Server.Transfer("Principal.aspx");
                //MessageBox.Show(resultado, "mensaje", MessageBoxButtons.OK);
                // this.Close();

            }
            else
            {
                string script = "alert('Contraseña y/o usuarios incorrectos');";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);

                // MessageBox.Show("ACCESO DENEGADO, INTENTE DE NUEVO", "ERROR", MessageBoxButtons.OK);
                txtusuario.Text = "";
                txtcontraseña.Text = "";
                txtusuario.Focus();

            }
        }
    }
}