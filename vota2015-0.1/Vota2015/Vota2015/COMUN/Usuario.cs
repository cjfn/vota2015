﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vota2015.COMUN
{
    public class Usuario
    {
        public Usuario()
        { }

        private string correo;

        public string Correo1
        {
            get { return correo; }
            set { correo = value; }
        }
        private string contraseña;

        public string Contraseña
        {
            get { return contraseña; }
            set { contraseña = value; }
        }

        public Usuario(string correo, string contraseña)
        {
            this.Correo1 = correo;
            this.Contraseña = contraseña;
        }
    }
}