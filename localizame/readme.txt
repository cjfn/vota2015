IMPORTANTE:  Antes de ejecutar el ejercicio, permitir la geolocalización en el navegador si éste la pregunta o permitirlo en los ajustes del mismo. 
					  En los Smartphone activar también la geolocalización para los navegadores.

Ejercicio probado en:

Linux(Fedora 19): 
	
	Firefox: 		No muestra el mapa y aparece un alert()," Error de Geolocalización. No se pudo determinar la ubicación actual ". Aún permitiendo la geolocalización en el navegador.
	Chrome: 		Funciona correctamente.

Windows Vista:

	Firefox: 		Funciona correctamente.
	Explorer:		No muestra el mapa y aparece un alert()," Error de Geolocalización. No se pudo determinar la ubicación actual ". Aún permitiendo la geolocalización en el navegador.

Windows 7:

	Firefox: 		Funciona correctamente.
	Chrome:		Funciona correctamente.
	Opera:			Funciona correctamente.
	Explorer:		No muestra el mapa y aparece un alert()," Error de Geolocalización. No se pudo determinar la ubicación actual ". Aún permitiendo la geolocalización en el navegador.
	
	
Smartphone:

	Iphone: 
  		
		Chrome: 	Funciona correctamente.
		Safari:    	Funciona correctamente.

	Android:
		
		Chrome: 	Funciona correctamente.
