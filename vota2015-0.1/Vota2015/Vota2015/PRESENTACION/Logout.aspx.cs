﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Vota2015.PRESENTACION
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["username"] = null;
            Response.Redirect("Default.aspx");
            string script = "confirm('Saliendo...');";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);

        }
    }
}