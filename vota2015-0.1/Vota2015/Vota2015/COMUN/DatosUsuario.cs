﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Vota2015.COMUN
{
    public class DatosUsuario
    {
         static SqlConnection conexion = new SqlConnection();
        public static void AbrirConexion()
        {
            conexion.ConnectionString = @"Data Source=.\SQLExpress; initial catalog=vota2015; Integrated Security=Yes";
            conexion.Open();
        }

        public static void CerrarConexion()
        {
            conexion.Close();
        }

        public static string iniciarSesion(string usuario, string contraseña)
        {
            int logueado = 0;
            string mensaje = "";
            AbrirConexion();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "logear";// procedimiento almacenado
            cmd.Connection = conexion;
            cmd.CommandType = CommandType.StoredProcedure;
            //PARAMETROS
            cmd.Parameters.Add(new SqlParameter("@usuario", usuario));
            cmd.Parameters.Add(new SqlParameter("@contraseña", contraseña));


            // parametros de salida
            SqlParameter pLogueado = new SqlParameter("@logueado", 0);
            pLogueado.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(pLogueado);

            SqlParameter pMensaje = new SqlParameter("@mensaje", SqlDbType.VarChar);
            pMensaje.Direction = ParameterDirection.Output;
            pMensaje.Size = 40;
            cmd.Parameters.Add(pMensaje);

            //cerramos conexion
            cmd.ExecuteNonQuery();
            CerrarConexion();

            logueado = Int32.Parse(cmd.Parameters["@logueado"].Value.ToString());
            if (logueado > 0)
            {
                mensaje = (cmd.Parameters["@mensaje"].Value.ToString());
                return mensaje;

            }
            else
                return mensaje;


        }

        public static string buscarcandidato(int partido, int puesto)
        {
            
            string mensaje = "";
            AbrirConexion();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "selectcandidato";// procedimiento almacenado
            cmd.Connection = conexion;
            cmd.CommandType = CommandType.StoredProcedure;
            //PARAMETROS
            cmd.Parameters.Add(new SqlParameter("@partido", partido));
            cmd.Parameters.Add(new SqlParameter("@puesto", puesto));


            // parametros de salida

            //cerramos conexion
            cmd.ExecuteNonQuery();
            CerrarConexion();

            return mensaje;


        }
    }

}