﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Vota2015.COMUN;
using Vota2015.NEGOCIOS;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;


namespace Vota2015.PRESENTACION
{
    public partial class HacerVoto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                lblusuario.Text = Session["username"].ToString();


                if (!IsPostBack)
                {
                    ddlpartido.DataSource = GetData("selectpartido", null);
                    ddlpartido.DataBind();


                    ddlpuesto.DataSource = GetData("selectpuestocandidato", null);
                    ddlpuesto.DataBind();

                    ddlcandidato.DataSource = GetData("selectcandidatos", null);
                    ddlcandidato.DataBind();

                    ListItem LiPartido = new ListItem("Seleccione Partido", "-1");
                    ddlpartido.Items.Insert(0, LiPartido);

                    ListItem LiPuesto = new ListItem("Seleccione Puesto", "-1");
                    ddlpuesto.Items.Insert(0, LiPuesto);

                    ListItem Licandidato = new ListItem("Seleccione Candidato", "-1");
                    ddlcandidato.Items.Insert(0, Licandidato);

                    
                    



                }
            }
            }


        private DataSet GetData(string SPName, SqlParameter SPParameter)
        {
            string CS = ConfigurationManager.ConnectionStrings["conn2"].ConnectionString;
            SqlConnection con = new SqlConnection(CS);
            SqlDataAdapter da = new SqlDataAdapter(SPName, con);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            if (SPParameter != null)
            {
                da.SelectCommand.Parameters.Add(SPParameter);
            }

            DataSet DS = new DataSet();
            da.Fill(DS);

            return DS;
        }

        

        public bool logueado = false;
        protected void Button1_Click(object sender, EventArgs e)
        {


        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string script = "confirm('Bienvenido');";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            Server.Transfer("Logout.aspx");
        }

        protected void ddlpuesto_SelectedIndexChanged(object sender, EventArgs e)
        {
            //String resultado = DatosUsuario.iniciarSesion(txtusuario.Text, txtcontraseña.Text);

            
;

           
            /*ddlcandidato.Enabled = true;
            SqlParameter parameter = new SqlParameter("@partido", ddlpartido.SelectedValue);
            SqlParameter parameter1 = new SqlParameter("@puesto", ddlpartido.SelectedValue);
            DataSet Ds = GetData("selectcandidato", (parameter) );

            ddlcandidato.DataSource = Ds;
            ddlcandidato.DataBind();

            ListItem LiCandidato = new ListItem("seleccione candidato", "-1");
            ddlcandidato.Items.Insert(1, LiCandidato);*/


        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            NegocioIngresoUsuarios registro_votos = new NegocioIngresoUsuarios();
            if(registro_votos.Registro_voto(lblusuario.Text, int.Parse(ddlpartido.SelectedIndex.ToString()), int.Parse(ddlpuesto.SelectedIndex.ToString()), int.Parse(ddlcandidato.SelectedIndex.ToString()), int.Parse(txtvotos.Text))> 0)
            {
                string script = "alert('ERROR EN EL INGRESO');";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                txtvotos.Text = "";
                     

           
            }

            {
                string script = "alert('DATOS INGRESADOS CORRECTAMENTE, GRACIAS');";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                ddlpartido.DataSource = GetData("selectpartido", null);
                ddlpartido.DataBind();


                ddlpuesto.DataSource = GetData("selectpuestocandidato", null);
                ddlpuesto.DataBind();

                ddlcandidato.DataSource = GetData("selectcandidatos", null);
                ddlcandidato.DataBind();

                ListItem LiPartido = new ListItem("Seleccione Partido", "-1");
                ddlpartido.Items.Insert(0, LiPartido);

                ListItem LiPuesto = new ListItem("Seleccione Puesto", "-1");
                ddlpuesto.Items.Insert(0, LiPuesto);

                ListItem Licandidato = new ListItem("Seleccione Candidato", "-1");
                ddlcandidato.Items.Insert(0, Licandidato);

            }
        }

        

       
    }
}