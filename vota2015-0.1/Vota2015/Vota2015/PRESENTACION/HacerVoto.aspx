﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HacerVoto.aspx.cs" Inherits="Vota2015.PRESENTACION.HacerVoto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>VOTACION</title>
    
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<link rel="stylesheet" href="css/bootstrap.min.css"/>
    
<link rel="stylesheet" href="miestilo.css" />
    <link rel="icon" type:"image/png" href="IMGS/icon.png" />


      <script src="js/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.quicksearch.js" type="text/javascript"></script>
  

</head>
<body >
     <header>
         
         
    
<div class="container" class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
 
         <div id="title" class="container" class="col-xs-4 col-sm-4 col-md-12 col-lg-12">
    
    <br />
             <br />
             <br />           
<h1>FORMULARIO DE VOTACION </h1>                                 
        
                    
            </div>
      <div id="header">
                <ul class="nav">
					<li><a href="Default.aspx">HOME</a></li>
                    <li><a href="GraficasPresidente.aspx">RESULTADOS PRESIDENTE</a></li>
                    <li><a href="Ingreso.aspx">INGRESAR</a></li>
                    <li><a href="Documentacion.aspx">DOCUMENTACION</a>
                    </li>
                         
                       
                   
                    <li><a href="sobrenosotros.aspx">SOBRE NOSOTROS</a> </li>
                        </ul>
        
                    
                                 
                    
            </div>
    <br />
    <br />
    <br />
    <br />
    <br />
             <h4>PARA REALIZAR EL CONTEO DE VOTOS USTED DEBERA DE LLENAR UN FORMULARIO</h4>


</div>

       
        
     </header>
    <form id="form1" runat="server">
<section class="main row">
<article class="col-xs-4 col-sm-3 col-md- col-lg-6">

    
    
       <div id="log">
              <div id="titulo4">
       BIENVENIDO
   </div>
           <br />
           
     <table class="auto-style1">
        <tr>
            <asp:Label ID="lblusuario" runat="server" Text="Label"></asp:Label><br />
            <asp:Button ID="Button2" runat="server" Text="CERRAR SESION" OnClick="Button2_Click" ></asp:Button>

        </tr>

         <tr>
            <h6>En esta opcion usted debera de llenar el formulario de registro con los datos del partido y el numero de votos</h6>
        </tr>
    </table>

           </div>
        
    </article>
     <div id="title2" class="col-xs-6 col-sm-3 col-md- col-lg-6">
       <div id="lateral">
              <div id="titulo5">  
         <h1>FORMULARIO DE VOTACION</h1>
                  </div>
           <br />
         <table class="nav-justified">
             <tr>
                 <td>
                     <asp:Label ID="Label1" runat="server" Text="Seleccione partido:"></asp:Label>
                 </td>
                 <td>
                     <asp:DropDownList ID="ddlpartido" runat="server" DataTextField="nombre_partido" DataValueField="id_partido" >
                     </asp:DropDownList>
                 </td>
                 <td>&nbsp;</td>
             </tr>
             <tr>
                 <td>
                     <asp:Label ID="Label2" runat="server" Text="Seleccione puesto:"></asp:Label>
                 </td>
                 <td>
                     <asp:DropDownList ID="ddlpuesto" runat="server" DataTextField="nombre_candidatura" DataValueField="id_candidatura_puesto" OnSelectedIndexChanged="ddlpuesto_SelectedIndexChanged">
                     </asp:DropDownList>
                 </td>
                 <td>&nbsp;</td>
             </tr>
             <tr>
                 <td>
                     <asp:Label ID="Label3" runat="server" Text="Seleccione Nombre del candidato:"></asp:Label>
                 </td>
                 <td>
                     <asp:DropDownList ID="ddlcandidato" runat="server" DataTextField="nombre" DataValueField="idcandidato" >
                     </asp:DropDownList>
                 </td>
                 <td>&nbsp;</td>
             </tr>
             <tr>
                 <td>
                     <asp:Label ID="Label4" runat="server" Text="Ingrese la cantidad de votos:"></asp:Label>
                 </td>
                 <td>
                     <asp:TextBox ID="txtvotos" runat="server"></asp:TextBox>
                 </td>
                 <td>&nbsp;</td>
             </tr>
             <tr>
                 <td>&nbsp;</td>
                 <td>
                     <asp:Button ID="Button1" runat="server" Text="INGRESAR VOTACION" OnClick="Button1_Click1" />
                 </td>
                 <td>&nbsp;</td>
             </tr>
             <tr>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
             </tr>
             <tr>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
             </tr>
             <tr>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
             </tr>
         </table>
        

        </div>
         </div>
    
    </section>

    <footer>
        <div id="titulo2" class="container" class="col-xs-4 col-sm-4 col-md-12 col-lg-12">
    <div id="header">
               
<h1>CONTACTO </h1>                                 
        
                    
            </div>
      
             <h5>cjfn10101@gmail.com - tel. +50230154416</h5>


</div>

       
    </footer>
        </form>
</body>
</html>
