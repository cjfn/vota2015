﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Vota2015.PRESENTACION
{
    public partial class BUSCAR : System.Web.UI.Page
    {
        Da_Rol clsRol = new Da_Rol();
        protected void Page_Load(object sender, EventArgs e)
        {
             
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                lblusuario.Text = Session["username"].ToString();

            if (!Page.IsPostBack)
            {
                f_listar();
            }
            }

        }
        protected void f_listar()
        {
            lstRol.DataSource = clsRol.funcionListar();
            lstRol.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}