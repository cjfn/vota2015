﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ingreso.aspx.cs" Inherits="Vota2015.PRESENTACION.Ingreso" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>INGRESO</title>
     <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<link rel="stylesheet" href="css/bootstrap.min.css"/>
    
<link rel="stylesheet" href="miestilo.css" />
    <link rel="icon" type:"image/png" href="IMGS/icon.png" />


      <script src="js/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.quicksearch.js" type="text/javascript"></script>
  
</head>
<body>
       <header>
         <div id="title" class="container" class="col-xs-4 col-sm-4 col-md-12 col-lg-12">
    <div id="header">
               
<h1>REGISTRESE </h1>                                 
        
                    
            </div>
      
             <h5>PARA PODER HACER SU CONTEO DE VOTOS, DEBERA DE ESTAR  REGISTRADO LEGALMENTE EN EL SISTEMA</h5>


</div>

       
        
     </header>

<section class="main row">
<article class="col-xs-4 col-sm-3 col-md- col-lg-6">

    
    <form id="form1" runat="server">
       <div id="log">
              <div id="titulo4">
       REGISTRATE
   </div>
           <br />
           
     <table class="auto-style1">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Usuario:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtusuario" runat="server" Width="200px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Contraseña:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtcontraseña" runat="server" TextMode="Password" Width="200px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Ingreso" OnClick="Button1_Click" />
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
           </div>
        </form>
    </article>

    <div id="title2" class="col-xs-6 col-sm-3 col-md- col-lg-6">
        <img src="IMGS/VOTA15.png" width="250"/>
        </div>
    </section>

    <footer>
        <div id="titulo2" class="container" class="col-xs-4 col-sm-4 col-md-12 col-lg-12">
    <div id="header">
               
<h1>CONTACTO </h1>                                 
        
                    
            </div>
      
             <h5>cjfn10101@gmail.com - tel. +50230154416</h5>


</div>

       
    </footer>
</body>
</html>
