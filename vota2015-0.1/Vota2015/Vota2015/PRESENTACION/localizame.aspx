﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="localizame.aspx.cs" Inherits="Vota2015.PRESENTACION.localizame" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="localizame/zepto.min.js"></script>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="localizame/geoM8.js"></script>
    <link href="localizame/miestilo.css" rel="stylesheet" />
</head> 
<body>
    <form id="form1" runat="server">
    <div>
     <header id="hea"><h3 id="encabezado">Geolocalización Firefox OS</h3></header>
   <nav id="nav">
     <button type="button" id="compacta" class="button">Compactar</button>
   </nav>
   <article id="map"></article>
   <footer id="foo"></footer>
    </div>
    </form>
</body>
</html>
