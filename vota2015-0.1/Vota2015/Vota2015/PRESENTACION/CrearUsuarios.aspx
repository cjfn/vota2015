﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrearUsuarios.aspx.cs" Inherits="Vota2015.PRESENTACION.CrearUsuarios" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CrearUsuarios</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<link rel="stylesheet" href="css/bootstrap.min.css"/>

    
<link rel="stylesheet" href="miestilo.css" />
    <link rel="icon" type:"image/png" href="IMGS/icon.png" />
    </head>
<body>
    <form id="form1" runat="server">
     <header>
         
    
<div class="container" class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
    <div id="header">
                <ul class="nav">
					<li><a href="Default.aspx">HOME</a></li>
                    <li><a href="CrearUsuarios.aspx">CREE USUARIOS</a></li>
                    <li><a href="BuscarUsuarios.aspx">BUSQUE USUARIOS</a>
                    <ul>
                        <li><a href="BuscarUsuariosDpi.aspx">BUSQUE POR DPI</a></li>
                        <li><a href="BusquedasAvanzadas.aspx">BUSQUEDAS AVANZADAS</a></li>
                     </ul>
                    <li><a href="Documentacion.aspx">DOCUMENTACION</a>
                    </li>
                   
             
                       
                   
                    <li><a href="sobrenosotros.aspx">SOBRE NOSOTROS</a> </li>
                        
                    
                                 
                    
            </div>
      
<img src="IMGS/PRINCIPAL1.png" />
    <img src="IMGS/PRINCIAPL2.png" />


</div>
       
        




</header>
        <div class="container">
            <br />
            <img src="IMGS/partidos/PNG/ANN50.png" /><img src="IMGS/partidos/PNG/CNN50.png" /><img src="IMGS/partidos/PNG/CREA50.png" /><img src="IMGS/partidos/PNG/ENCUENTRO50.png" /><img src="IMGS/partidos/PNG/GANA50.png" /><img src="IMGS/partidos/PNG/LIDER50.png" /><img src="IMGS/partidos/PNG/MNR50.png" /><img src="IMGS/partidos/PNG/MR50.png" /><img src="IMGS/partidos/PNG/PAN50.png" /><img src="IMGS/partidos/PNG/PATRIOTA50.png" /><img src="IMGS/partidos/PNG/PRI50.png" /><img src="IMGS/partidos/PNG/TODOS50.png" /><img src="IMGS/partidos/PNG/UCN50.png" /><img src="IMGS/partidos/PNG/UNE50.png" /><img src="IMGS/partidos/PNG/UNIONISTA50.png" /><img src="IMGS/partidos/PNG/URNG50.png" /><img src="IMGS/partidos/PNG/VICTORIA50.png" /><img src="IMGS/partidos/PNG/VIVA50.png" />
            <br />
            </div>

<div class="container">
    <br />
  
<section class="main row">
<article class="col-xs-12 col-sm-8 col-md- col-lg-9">

    
    <div id="title">
    <center>  <h2>REGISTRO DE USUARIO</h2></center>
        </div>
   
    <div id="principal">
      <h1>¿QUE PUEDO HACER?</h1>
        <p>En el sistema Vota2015 usted puede administrar usuarios, asignar permisos a usuarios para que estos puedan hacer uso del sistema</p>
      
 
         <div id="title">FORMULARIO DE INGRESO </div>
        
        <div id="principal">


            <table class="nav-justified">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="nombre:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtnombre" runat="server" Width="300px"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="apellido:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtapellido" runat="server" Width="300px"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="dpi"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtdpi" runat="server"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label5" runat="server" Text="correo:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtcorreo" runat="server" Width="200px"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label6" runat="server" Text="contraseña:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtcontraseña" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label7" runat="server" Text="cn contraseña:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtconfirmarcontraseña" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label8" runat="server" Text="telefono:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txttelefono" runat="server"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label9" runat="server" Text="departamento:"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddldepartamento" runat="server"  DataTextField="nombre_departamento" DataValueField="id_departamento"  OnSelectedIndexChanged="ddldepartamento_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label14" runat="server" Text="Municipio:"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlmunicipio" runat="server" OnSelectedIndexChanged="ddlmunicipio_SelectedIndexChanged"  DataTextField="municipio" DataValueField="idmunicip" >
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        Dirección: </td>
                    <td>
                        <asp:TextBox ID="txtdireccion" runat="server"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label10" runat="server" Text="fecha de nacimiento:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtnacimiento" runat="server"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label13" runat="server" Text="puesto:"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlpuesto" runat="server"  DataTextField="nombre" DataValueField="id_puesto" >
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Ingreso" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Borrar" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>


        </div>
</article>
<aside class="col-xs-12 col-sm-4 col-md-3 col-lg-3" style="background:white;">

    <br />
    
  
     <br />
    <center>
        <div id="title">
         <asp:Label ID="Label1" runat="server" Text="BIENVENIDO"></asp:Label>
            </div>
         

        <div id="principal">
<asp:Label ID="lblusuario" runat="server" Text="Label"></asp:Label>
<asp:Button ID="Button1" runat="server" Text="CERRAR SESION" OnClick="Button1_Click"></asp:Button>
        </div>
  </center>


</aside>



</section>
        
    <br />
</div>
<footer>
    <br />
<div class="row">

<div class="color1 col-xs-12 col-sm-12 col-md-12">
<center>
<h1>CONTACTO</h1>
</center>
<p>
<center>
    
Derechos reservados  <br />
    correo:cjfn10101@gmail.com <br />
    Tel(502) 30154416
</center>


</p>
    </div>

    </div>
  </footer>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
    </form>
</body>
</html>
