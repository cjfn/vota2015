﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GraficasPresidente.aspx.cs" Inherits="Vota2015.PRESENTACION.GraficasPresidente" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PRESIDENTE</title>

     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<link rel="stylesheet" href="css/bootstrap.min.css"/>

    
<link rel="stylesheet" href="miestilo.css" />
    <link rel="icon" type:"image/png" href="IMGS/icon.png" />

</head>
<body>

    <header>
         
    
<div class="container" class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
    <div id="header">
                <ul class="nav">
					<li><a href="Default.aspx">HOME</a></li>
                    <li><a href="CrearUsuarios.aspx">PRESIDENTES</a></li>
                    <li><a href="BuscarUsuarios.aspx">DIPUTADOS</a>
                    
                    <li><a href="Documentacion.aspx">ALCALDES</a>
                    </li>
                   
             
                       
                   
                    <li><a href="sobrenosotros.aspx">SOBRE NOSOTROS</a> </li>
                        
                    
                                 
                    
            </div>
      
<img src="IMGS/PRINCIPAL1.png" />
    <img src="IMGS/PRINCIAPL2.png" />


</div>
       
        




</header>

    <div class="container">
            <br />
            <img src="IMGS/partidos/PNG/ANN50.png" /><img src="IMGS/partidos/PNG/CNN50.png" /><img src="IMGS/partidos/PNG/CREA50.png" /><img src="IMGS/partidos/PNG/ENCUENTRO50.png" /><img src="IMGS/partidos/PNG/GANA50.png" /><img src="IMGS/partidos/PNG/LIDER50.png" /><img src="IMGS/partidos/PNG/MNR50.png" /><img src="IMGS/partidos/PNG/MR50.png" /><img src="IMGS/partidos/PNG/PAN50.png" /><img src="IMGS/partidos/PNG/PATRIOTA50.png" /><img src="IMGS/partidos/PNG/PRI50.png" /><img src="IMGS/partidos/PNG/TODOS50.png" /><img src="IMGS/partidos/PNG/UCN50.png" /><img src="IMGS/partidos/PNG/UNE50.png" /><img src="IMGS/partidos/PNG/UNIONISTA50.png" /><img src="IMGS/partidos/PNG/URNG50.png" /><img src="IMGS/partidos/PNG/VICTORIA50.png" /><img src="IMGS/partidos/PNG/VIVA50.png" />
            <br />
            </div>

<div class="container">
    <br />
  
<section class="main row">
<article class="col-xs-12 col-sm-8 col-md- col-lg-9">

    
    <div id="title">
    <center>  <h2>GRAFICAS</h2></center>
        </div>
   
    <div id="principal">
      <h1>RESULTADOS DE VOTOS POR PRESIDENTE</h1>
      
 
    <form id="form1" runat="server">
        







    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
        <div>

            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Height="494px" Width="681px">
                <LocalReport ReportPath="PRESENTACION\graficapresidentes.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="SqlDataSource2" Name="DataSet1" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:conn2 %>" SelectCommand="resultadospresidente" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
    </div>
    
</article>
<aside class="col-xs-12 col-sm-4 col-md-3 col-lg-3" style="background:white;">

    <center>
        
        
                    


        
          <div id="title">
         <asp:Label ID="Label2" runat="server" Text="BIENVENIDO"></asp:Label>
            </div>
         

        <div id="principal">
<asp:Label ID="lblusuario" runat="server" Text="Label"></asp:Label>
<asp:Button ID="Button1" runat="server" Text="CERRAR SESION" OnClick="Button1_Click"></asp:Button>
        </div>
          </center>

    <div id="title"> PARTIDOS </div>

    <div id="principal">
        <p>1. PARTIDO DE AVANZAD</p> 
        <p>2. MOVIMIENTO REFORMADOR</p> 
        <p>3. UNIDAD REVOLUCIONARIA NACIONAL GUATEMALTECA</p> 
        <p>4. Partido Patriota</p> 
        <p>5. Gran Alianza Nacional</p> 
        <p>6. Unidad Nacional de la esperanza </p>
        <p>7. Partido Unionista</p> 
        <p>8. Union del Cambio Nacional</p> 
        <p>9. Encuentro por Guatemala</p> 
        <p>10. Visión con Valores</p> 
        <p>11. Frente de Convergencia Nacional</p> 
        <p>12. Alternativa Nueva Nación</p> 
        <p>13. Compromiso, Renovación y Orden</p> 
        <p>14. Libertad Democratica Renovada</p> 
        <p>15. Partido Victoria</p> 
        <p>16. Corazón Nueva Nación</p> 
        <p>17. WINAQ</p> 
        <p>18. Moviemiento Nueva Republica</p> 
        <p>20. Partido Republicano Institucional</p> 
        
       </div>


    </form>
</aside>
    


</section>
        
    <br />
</div>
<footer>
    <br />
<div class="row">

<div class="color1 col-xs-12 col-sm-12 col-md-12">
<center>
<h1>CONTACTO</h1>
</center>
<p>
<center>
    
Derechos reservados  <br />
    correo:cjfn10101@gmail.com <br />
    Tel(502) 30154416
</center>


</p>
    </div>

    </div>
  </footer>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
   
    
</body>
</html>
