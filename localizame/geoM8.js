$(function(){

   $("#foo").html("Fecha: " + new Date());

	var map, lat, lng, inicio;

	function enlazarMarcador(e) {
		map.drawRoute({																//Ruta entre marca anterior y actual
			origin: [lat, lng],																//coordenadas anteriores
			destination: [ e.latLng.lat(), e.latLng.lng() ], 		//destino coordenadas realizadas con tap o click
			travel_mode:'driving', 													//modo de viaje
			strokeColor: '#000000', 												//color de ruta
			strokeOpacity: 0.6,       													//opacidad de ruta
			strokeWeight:5																	//ancho en pixeles
		});
		lat = e.latLng.lat(); 															//guardo coordenadas para marca siguiente
		lng = e.latLng.lng();

		map.addMarker({ lat: lat, lng: lng}); 						//Marcador en el mapa
	};

	function geolocalizar() {
		GMaps.geolocate({
			   success: function (position) {
				lat = position.coords.latitude;								//Guarda coordenadas en lat y lng
				lng = position.coords.longitude;

				inicio = [ lat, lng ]; 														//Guardo las coordenadas iniciales para la función compactar

				map = new GMaps({												//Muestra mapa con las coordenadas guardadas en lat y lng
					el: "#map",
					lat: lat,
					lng: lng,
					click: enlazarMarcador,											//evento click para el navegador
					tap: enlazarMarcador												//evento táctil
			 });
			 map.addMarker({ lat: lat, lng: lng });					//marcador con latitud y longitud
			},
			error: function (error) { alert('Error de Geolocalización: '+error.message);},
			not_supported: function(){alert('Tu navegador no soporta geolocalización');},
		});
   };

   function compactar() {
   	map.cleanRoute();																//borra rutas y marcadores
   	map.removeMarkers();

   	map.addMarker({ lat: inicio[0], lng: inicio[1] }); 				//Marcador inicial

   	map.drawRoute({
   		origin: inicio,																		//coordenadas de origen
			destination: [ lat, lng ], 												//destino coordenadas realizadas con tap o click
			travel_mode:'driving', 													//modo de viaje
			strokeColor: '#000000', 												//color de ruta
			strokeOpacity: 0.6,       													//opacidad de ruta
			strokeWeight: 5
   	});
   	map.addMarker({ lat: lat, lng: lng });						//situa el marcador final
   };

   geolocalizar();

   $("#compacta").click(compactar); 									//evento para botón o tap
});

	