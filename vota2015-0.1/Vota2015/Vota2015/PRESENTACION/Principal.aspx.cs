﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Vota2015.PRESENTACION
{
    public partial class Principal : System.Web.UI.Page
    {
        public bool logueado = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                string script = "alert('Bienvenido');";
                          ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
                
                lblusuario.Text = Session["username"].ToString();
                
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

           
            string script = "confirm('Bienvenido');";
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);
            Server.Transfer("Logout.aspx");
        }
    }
}