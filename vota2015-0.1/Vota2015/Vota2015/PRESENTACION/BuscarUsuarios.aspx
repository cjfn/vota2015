﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BuscarUsuarios.aspx.cs" Inherits="Vota2015.PRESENTACION.BUSCAR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BUSCAR</title>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<link rel="stylesheet" href="css/bootstrap.min.css"/>
    
<link rel="stylesheet" href="miestilo.css" />
    <link rel="icon" type:"image/png" href="IMGS/icon.png" />


      <script src="js/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="js/jquery.quicksearch.js" type="text/javascript"></script>
      <script type="text/javascript">
          $(document).ready(function () {
              var txtFiltro = '#' + '<%=txtFiltro.ClientID %>';
              var grillaJedis = '#' + 'tbl_rol';
              $(txtFiltro).quicksearch(grillaJedis + ' tbody tr');
          });
    </script>
</head>
<body>

    <header>
         
    
<div class="container" class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
    
      
<img src="IMGS/PRINCIPAL1.png" />
    <img src="IMGS/PRINCIAPL2.png" />


</div>
       
    <div id="header">
                <ul class="nav">
					<li><a href="Default.aspx">HOME</a></li>
                    <li><a href="CrearUsuarios.aspx">CREE USUARIOS</a></li>
                    <li><a href="BuscarUsuarios.aspx">BUSQUE USUARIOS</a>
                    <ul>
                        <li><a href="BuscarUsuariosDpi.aspx">BUSQUE POR DPI</a></li>
                        <li><a href="BusquedasAvanzadas.aspx">BUSQUEDAS AVANZADAS</a></li>
                     </ul>
                    <li><a href="Documentacion.aspx">DOCUMENTACION</a>
                    </li>
                   
             
                       
                   
                    <li><a href="sobrenosotros.aspx">SOBRE NOSOTROS</a> </li>
                        
                    
                                 
                    
            </div>    




</header>

    <div class="container">
            <br />
            <img src="IMGS/partidos/PNG/ANN50.png" /><img src="IMGS/partidos/PNG/CNN50.png" /><img src="IMGS/partidos/PNG/CREA50.png" /><img src="IMGS/partidos/PNG/ENCUENTRO50.png" /><img src="IMGS/partidos/PNG/GANA50.png" /><img src="IMGS/partidos/PNG/LIDER50.png" /><img src="IMGS/partidos/PNG/MNR50.png" /><img src="IMGS/partidos/PNG/MR50.png" /><img src="IMGS/partidos/PNG/PAN50.png" /><img src="IMGS/partidos/PNG/PATRIOTA50.png" /><img src="IMGS/partidos/PNG/PRI50.png" /><img src="IMGS/partidos/PNG/TODOS50.png" /><img src="IMGS/partidos/PNG/UCN50.png" /><img src="IMGS/partidos/PNG/UNE50.png" /><img src="IMGS/partidos/PNG/UNIONISTA50.png" /><img src="IMGS/partidos/PNG/URNG50.png" /><img src="IMGS/partidos/PNG/VICTORIA50.png" /><img src="IMGS/partidos/PNG/VIVA50.png" />
            <br />
            </div>

<div class="container">
    <br />
  
<section class="main row">
<article class="col-xs-12 col-sm-8 col-md- col-lg-9">
    
    <CENTER>
    <div id="title">
    <center>  <h2>BUSCAR USUARIO</h2></center>
        </div>
    
    <form id="form1" runat="server">
        <asp:Label ID="Label2" runat="server" Text="INGRESE DATOS PARA BUSQUEDA:"></asp:Label>
    <asp:TextBox runat="server" ID="txtFiltro"></asp:TextBox>
    
    <div>
        <table id="tbl_rol" border="1">
            <thead>
                <tr>
                    <th>
                       NOMBRE
                    </th>
                    <th>
                        APELLIDO
                    </th>
                    <th>
                        DPI
                    </th>
                </tr>
            </thead>
            <tbody>
                <asp:ListView runat="server" ID="lstRol">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%#Eval("nombre_usuario") %>
                            </td>
                            <td>
                                <%#Eval("apellido_usuario") %>
                            </td>
                            <td>
                                <%#Eval("dpi") %>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </tbody>
        </table>
    </div>
        
    </form>
        </CENTER>
    </article>

    <aside class="col-xs-12 col-sm-4 col-md-3 col-lg-3" style="background:white;">

    <br />
    
  
     <br />
    <center>
        <div id="title">
         <asp:Label ID="Label1" runat="server" Text="BIENVENIDO"></asp:Label>
            </div>
         

        <div id="principal">
            

<asp:Label ID="lblusuario" runat="server" Text="Label"></asp:Label>

            
        </div>
  </center>


</aside>



</body>
</html>
