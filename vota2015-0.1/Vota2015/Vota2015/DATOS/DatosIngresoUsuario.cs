﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using Vota2015.COMUN;
using System.Configuration;

namespace Vota2015.DATOS
{
    public class DatosIngresoUsuario
    {
        public DatosIngresoUsuario()
        { }

        public static string constr
        {
            get { return ConfigurationManager.ConnectionStrings["conn2"].ConnectionString; }
        }

        public static string Provider //proveedor de diferentes tipos de bases de datos
        {
            get { return ConfigurationManager.ConnectionStrings["conn2"].ProviderName; }
        }

        public static DbProviderFactory dpf
        {
            get { return DbProviderFactories.GetFactory(Provider); }
        }

        public static int ejecutaNonQuery(string StoredProcedure, List<DbParameter> parametros)
        {
            int Id = 0;
            try
            {
                using (DbConnection con = dpf.CreateConnection())
                {
                    con.ConnectionString = constr;

                    using (DbCommand cmd = dpf.CreateCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandText = StoredProcedure;
                        cmd.CommandType = CommandType.StoredProcedure;

                        foreach (DbParameter param in parametros)
                            cmd.Parameters.Add(param);
                        con.Open();
                        Id = cmd.ExecuteNonQuery();

                    }


                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                //coneccion cerrada
            }
            return Id;
        }

        public int IngresoUsuario(string nombre, string apellido, int dpi, string correo, string contraseña, int telefono, int departamento, int municipio, string direccion, string fecha_nac, int puesto)
        {

            List<DbParameter> parametros = new List<DbParameter>(); ;

            DbParameter param = dpf.CreateParameter();
            param.Value = nombre;
            param.ParameterName = "nombre";
            parametros.Add(param);

            DbParameter param1 = dpf.CreateParameter();
            param1.Value = apellido;
            param1.ParameterName = "apellido";
            parametros.Add(param1);

            DbParameter param2 = dpf.CreateParameter();
            param2.Value = dpi;
            param2.ParameterName = "dpi";
            parametros.Add(param2);

            DbParameter param3 = dpf.CreateParameter();
            param3.Value = correo;
            param3.ParameterName = "correo";
            parametros.Add(param3);

            DbParameter param4 = dpf.CreateParameter();
            param4.Value = contraseña;
            param4.ParameterName = "contraseña";
            parametros.Add(param4);

            DbParameter param5 = dpf.CreateParameter();
            param5.Value = telefono;
            param5.ParameterName = "telefono";
            parametros.Add(param5);

            DbParameter param6 = dpf.CreateParameter();
            param6.Value = departamento;
            param6.ParameterName = "departamento";
            parametros.Add(param6);

            DbParameter param7 = dpf.CreateParameter();
            param7.Value = municipio;
            param7.ParameterName = "municipio";
            parametros.Add(param7);

            DbParameter param8 = dpf.CreateParameter();
            param8.Value = direccion;
            param8.ParameterName = "direccion";
            parametros.Add(param8);

            DbParameter param9 = dpf.CreateParameter();
            param9.Value = fecha_nac;
            param9.ParameterName = "fecha_nac";
            parametros.Add(param9);


            DbParameter param10 = dpf.CreateParameter();
            param10.Value = puesto;
            param10.ParameterName = "puesto";
            parametros.Add(param10);

            return ejecutaNonQuery("addusuario", parametros);

        }


        // buscar todos los usuarios


        public List<BuscarsUsuariobyID> select_all_usuarios()
        {
            List<BuscarsUsuariobyID> LstUsuarios = new List<BuscarsUsuariobyID>();

            string StoredProcedure = "selectallusuarios";
            using (DbConnection con = dpf.CreateConnection())
            {
                con.ConnectionString = constr;
                using (DbCommand cmd = dpf.CreateCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = StoredProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    using (DbDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            LstUsuarios.Add(
                            new BuscarsUsuariobyID((int)dr["id_usuario"], (string)dr["nombre_usuario"], (string)dr["apellido_usuario"], (int)dr["dpi"]));
                        }
                    }
                }



            }
            return LstUsuarios;


        }

        // buscar 
        public BuscarsUsuariobyID select_usuariosbyid(int iduser, string nombre, string apellido, int dpi)
        {
            BuscarsUsuariobyID ObjUsuario = new BuscarsUsuariobyID();
            string StoredProcedure = "selectusuariosbyid";
            using (DbConnection con = dpf.CreateConnection())
            {
                con.ConnectionString = constr;
                using (DbCommand cmd = dpf.CreateCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = StoredProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DbParameter param = cmd.CreateParameter();
                    param.DbType = DbType.Int32;
                    param.Value = "iduser";
                    param.Value = iduser;
                    cmd.Parameters.Add(param);
                    con.Open();
                    using (DbDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            ObjUsuario = new BuscarsUsuariobyID(iduser, (string)dr["nombre"], (string)dr["apellido"], (int)dr["dpi"]);
                        }
                    }


                }
            }
            return ObjUsuario;
        }

        //buscar candidato
        public selectcandidatocomun buscarcandidatocomun(int partido, int puesto)
        {

            selectcandidatocomun ObjCandidato = new selectcandidatocomun();
            string StoredProcedure = "selectcandidato";
            using (DbConnection con = dpf.CreateConnection())
            {
                con.ConnectionString = constr;
                using (DbCommand cmd = dpf.CreateCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = StoredProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DbParameter param = cmd.CreateParameter();
                    param.DbType = DbType.Int32;
                    param.Value = "partido";
                    param.Value = partido;
                    cmd.Parameters.Add(param);

                    DbParameter param2 = cmd.CreateParameter();
                    param2.DbType = DbType.Int32;
                    param2.Value = "puesto";
                    param2.Value = puesto;
                    cmd.Parameters.Add(param2);
                    con.Open();

                    using (DbDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            ObjCandidato = new selectcandidatocomun(partido, puesto, (int)dr["cod"], (string)dr["nombre"], (string)dr["Apellido"]);
                            //ObjCandidato = new  BuscarsUsuariobyID(iduser, (string)dr["nombre"], (string)dr["apellido"], (int)dr["dpi"]);
                        }
                    }





                }
            }
            return ObjCandidato;
        }

        public int Registro_votacion(string usuario, int partido, int puesto, int candidato, int votos)
        {

            List<DbParameter> parametros = new List<DbParameter>(); ;

            DbParameter param = dpf.CreateParameter();
            param.Value = usuario;
            param.ParameterName = "usuario";
            parametros.Add(param);

            DbParameter param1 = dpf.CreateParameter();
            param1.Value = partido;
            param1.ParameterName = "partido";
            parametros.Add(param1);

            DbParameter param2 = dpf.CreateParameter();
            param2.Value = puesto;
            param2.ParameterName = "puesto";
            parametros.Add(param2);

            DbParameter param3 = dpf.CreateParameter();
            param3.Value = candidato;
            param3.ParameterName = "candidato";
            parametros.Add(param3);

            DbParameter param4 = dpf.CreateParameter();
            param4.Value = votos;
            param4.ParameterName = "votos";
            parametros.Add(param4);

            return ejecutaNonQuery("insert_registro_voto", parametros);

        }

    }
}