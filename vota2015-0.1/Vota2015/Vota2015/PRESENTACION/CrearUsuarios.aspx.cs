﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Vota2015.COMUN;
using Vota2015.NEGOCIOS;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace Vota2015.PRESENTACION
{
    public partial class CrearUsuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                lblusuario.Text = Session["username"].ToString();


                if (!IsPostBack)
                {
                    ddldepartamento.DataSource = GetData("selecdep", null);
                    ddldepartamento.DataBind();


                    ddlpuesto.DataSource = GetData("selectpuestos", null);
                    ddlpuesto.DataBind();

                    ListItem LiDepartamento = new ListItem("Seleccione Departamento", "-1");
                    ddldepartamento.Items.Insert(0, LiDepartamento);

                    ListItem LiMunicipio = new ListItem("Seleccione Municipio", "-1");
                    ddlmunicipio.Items.Insert(0, LiMunicipio);

                    ListItem LiPuesto = new ListItem("Seleccione Puesto", "-1");
                    ddlpuesto.Items.Insert(0, LiPuesto);

                    ddlmunicipio.Enabled = false;



                }
            }
        }
            private DataSet GetData(string SPName, SqlParameter SPParameter)
        {
            string CS = ConfigurationManager.ConnectionStrings["conn2"].ConnectionString;
            SqlConnection con = new SqlConnection(CS);
            SqlDataAdapter da = new SqlDataAdapter(SPName, con);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            if(SPParameter !=null)
            {
                da.SelectCommand.Parameters.Add(SPParameter);
            }

            DataSet DS = new DataSet();
            da.Fill(DS);

            return DS;
        }


        protected void Button2_Click(object sender, EventArgs e)
        {
            NegocioIngresoUsuarios negusuarios = new NegocioIngresoUsuarios();
            if (negusuarios.IngresoUsuarios(txtnombre.Text, txtapellido.Text, int.Parse(txtdpi.Text), txtcorreo.Text, txtcontraseña.Text, int.Parse(txttelefono.Text), int.Parse(ddldepartamento.SelectedIndex.ToString()), int.Parse(ddlmunicipio.SelectedIndex.ToString()), txtdireccion.Text, txtnacimiento.Text, int.Parse(ddlpuesto.SelectedIndex.ToString()))> 0) ;

            {
                string script = "alert('DATOS INGRESADOS CORRECTAMENTE, GRACIAS');";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);

                txtapellido.Text = "";
                txtconfirmarcontraseña.Text = "";
                txtcontraseña.Text="";
                txtcorreo.Text="";
                txtdireccion.Text="";
                txtdpi.Text="";
                txtnacimiento.Text="";
                txtnombre.Text = "";
                txttelefono.Text = "";
                ddldepartamento.DataSource = GetData("selecdep", null);
                ddldepartamento.DataBind();


                ddlpuesto.DataSource = GetData("selectpuestos", null);
                ddlpuesto.DataBind();

                ListItem LiDepartamento = new ListItem("Seleccione Departamento", "-1");
                ddldepartamento.Items.Insert(0, LiDepartamento);

                ListItem LiMunicipio = new ListItem("Seleccione Municipio", "-1");
                ddlmunicipio.Items.Insert(0, LiMunicipio);

                ListItem LiPuesto = new ListItem("Seleccione Puesto", "-1");
                ddlpuesto.Items.Insert(0, LiPuesto);

                ddlmunicipio.Enabled = false;


            }

            {
                string script = "alert('ERROR EN EL INGRESO');";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, true);

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        protected void ddlmunicipio_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void ddldepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlmunicipio.Enabled = true;
            SqlParameter parameter = new SqlParameter("@departamento", ddldepartamento.SelectedValue);
            DataSet DS = GetData("selmunicipio", parameter);
            ddlmunicipio.DataSource = DS;
            ddlmunicipio.DataBind();

            ListItem LiMunicipio = new ListItem("seleccione municipio", "-1");
            ddlmunicipio.Items.Insert(1, LiMunicipio);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {

            this.Controls.Clear();
        }
    }
}